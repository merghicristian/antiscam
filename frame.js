class Frame {
    constructor() { 
        this.frame = null
        this.appContainer = null

        this.appSettings = {}
    }

    initialize() {
        fetch(chrome.extension.getURL('views/main.html'))
            .then(response => response.text())
            .then(data => {
                document.querySelector('body').innerHTML = data

                this.appContainer = document.querySelector('#app_container')

                fetch(chrome.extension.getURL('views/report.html'))
                    .then(response => response.text())
                    .then(data => {
                        if (!data) return

                        this.frame = document.querySelector('.main__wrapper')
                        this.appContainer.innerHTML = data

                        this.initializeScamReport()
                        this.checkAPIKeyStatus()
                        this.setupListeners()
                    })
                    
                this.initializeNavigation()
            })
    }

    setupListeners() {
        chrome.storage.onChanged.addListener((changes) => {
            document.body.dataset.theme = changes.settings.newValue.lightTheme ? 'light' : 'dark'
        })
    }

    checkAPIKeyStatus() {
        const statusMessage = document.querySelector('#app_status')
        const statusRevokeBtn = document.querySelector('#app_revoke-btn')
        const statusHint = document.querySelector('.report__status_hint')

        statusMessage.textContent = 'Analyzing..'
        statusHint.textContent = 'Please, wait..'

        chrome.storage.local.get('settings', res => {
            this.appSettings = res.settings

            if (!this.appSettings.preventApiScam) {
                statusMessage.textContent = 'Protection disabled'
                statusMessage.classList.add('disabled')
                statusHint.textContent = 'You can enable protection in the settings'

                return
            }

            fetch('https://steamcommunity.com/dev/apikey')
                .then(response => response.text())
                .then(data => {
                    let nt = document.createElement('div')
                    nt.innerHTML = data

                    if (!nt.querySelector('#editForm input')) {
                        chrome.storage.local.set({'hasAPIKey': false})
                        statusMessage.textContent ='Waiting for Steam auth..'
                        statusHint.innerHTML = 'Go to <a href="https://steamcommunity.com/login" target="_blank">Steam</a> and login'
                    }
                    
                    if (nt.querySelector('#editForm input').type === 'submit') {
                        chrome.storage.local.set({'hasAPIKey': true})
                        statusMessage.textContent = 'Your account is in danger!'
                        statusMessage.classList.add('danger')
                        statusHint.innerHTML = '<a href="https://steamcommunity.com/dev/apikey" target="_blank">Revoke API-key</a>'
                    } else {
                        chrome.storage.local.set({'hasAPIKey': false})
                        statusMessage.textContent = 'You are safe!'
                        statusMessage.classList.add('safe')
                        statusHint.textContent = 'Be careful, use only our official website.'
                    }
                })
        })
    }

    initializeNavigation() {
        const navigationLinks = document.querySelectorAll('.main__h_link')

        navigationLinks.forEach(link => {
            link.addEventListener('click', () => {
                if (!link.classList.contains('selected')) {
                    navigationLinks.forEach(el => el.classList.remove('selected'))

                    link.classList.add('selected')

                    switch (link.textContent) {
                        case 'Report':
                            fetch(chrome.extension.getURL('views/report.html'))
                                .then(response => response.text())
                                .then(data => {
                                    if (!data) return

                                    this.appContainer.innerHTML = data

                                    this.checkAPIKeyStatus()
                                })

                            break

                        case 'Settings':
                            fetch(chrome.extension.getURL('views/settings.html'))
                                .then(response => response.text())
                                .then(data => {
                                    if (!data) return
        
                                    this.appContainer.innerHTML = data

                                    this.loadSettings()
                                })

                            break

                        case 'About':
                            fetch(chrome.extension.getURL('views/about.html'))
                                .then(response => response.text())
                                .then(data => {
                                    if (!data) return

                                    this.appContainer.innerHTML = data

                                    this.loadProjectStatus()
                                })

                            break
                    }
                }
            })
        })
    }

    initializeScamReport() {
        const reportField = document.querySelector('#app_reportfield')
        const reportButton = document.querySelector('#app_reportsend')
        const reportMsg = document.querySelector('#app_reportmsg')

        reportButton.addEventListener('click', () => {
            const exprUrl = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,8}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi

            if (reportField.value.length < 2) {
                reportMsg.textContent = 'Website URL too short'
                
                if (!reportMsg.classList.contains('error')) {
                    reportMsg.classList.add('error')

                    setTimeout(() => {
                        reportMsg.classList.remove('error')
                    }, 2000)
                } 

                return
            } else if (!exprUrl.test(reportField.value)) {
                reportMsg.textContent = 'Invalid website URL'
                
                if (!reportMsg.classList.contains('error')) {
                    reportMsg.classList.add('error')
    
                    setTimeout(() => {
                        reportMsg.classList.remove('error')
                    }, 2000)
                }

                return
            } else {
                fetch(config.hostname + '/check_scam_site', {
                    method: "POST",
                    body: JSON.stringify({'link': reportField.value}) })
                    .then(response => response.text())
                    .then(data => {
                        if (data == 'OK') {
                            reportMsg.textContent = 'This website will be checked. Thx!'
                
                            if (!reportMsg.classList.contains('success')) {
                                reportMsg.classList.add('success')
                                reportField.value = ''

                                setTimeout(() => {
                                    reportMsg.classList.remove('success')
                                }, 2000)
                            } 
                        } else {
                            reportMsg.textContent = 'Something went wrong..'
                
                            if (!reportMsg.classList.contains('error')) {
                                reportMsg.classList.add('error')
                                reportField.value = ''

                                setTimeout(() => {
                                    reportMsg.classList.remove('error')
                                }, 2000)
                            } 
                        }
                    })
                
            }
        })
    }

    loadSettings() {
        const switchers = document.querySelectorAll('.form-switch')

        chrome.storage.local.get('settings', res => {
            this.appSettings = res.settings
            
            if (!this.appSettings) {
                chrome.storage.local.set({'settings': {
                    preventScamSites: true,
                    preventApiScam: true,
                    liveAnalysis: false,
                    lightTheme: false
                }})
    
                this.loadSettings()
    
                return
            }
            
            switchers.forEach(item => {
                const checkbox = item.children[0]
    
                checkbox.checked = this.appSettings[checkbox.id]
    
                checkbox.addEventListener('change', (e) => {
                    this.appSettings[checkbox.id] = checkbox.checked
    
                    chrome.storage.local.set({'settings': this.appSettings})
                })
            })
        })
    }

    loadProjectStatus() {
        const csmIndicator = document.querySelector('#ind_csm')
        const antiIndicator = document.querySelector('#ind_anti')

        isSiteOnline('cs.money', status => {
            if (status) {
                csmIndicator.textContent = 'Online'
                csmIndicator.classList.add('online')
            } else {
                csmIndicator.textContent = 'Offline'
                csmIndicator.classList.add('offline')
            }
        })

        isSiteOnline('csgo.agency', status => {
            if (status) {
                antiIndicator.textContent = 'Online'
                antiIndicator.classList.add('online')
            }
        })

        function isSiteOnline(url, callback) {
            const image = document.body.appendChild(document.createElement("img"))

            image.onload = () =>
            {
                callback(true)
            }

            image.onerror = () =>
            {
                callback(false)
            }   

            image.src = `https://${url}/favicon.ico`
        }
    }
}

document.addEventListener('DOMContentLoaded', () => {
    const mainFrame = new Frame

    mainFrame.initialize()
})